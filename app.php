<?php

// The idea is that this file is executed from a console "php app.php".

require_once('autoload.php');

use Entity\Movie;
use Entity\Actor;

// This was to get this working quickly in my local environment.
ini_set('date.timezone', 'Europe/London');

// Create the movie.
$movie = new Movie();
$movie->setTitle('Test Movie');
$movie->setReleaseDate(new DateTime());
$movie->setRuntime(1000);

// The first actor.
$tomCruise = new Actor();
$tomCruise->setName('Tom Cruise');
$tomCruise->setDateOfBirth(new DateTime('2011-09-01 00:00:00'));

// The second actor.
$mattDamon = new Actor();
$mattDamon->setName('Matt Damon');
$mattDamon->setDateOfBirth(new DateTime('2010-08-02 00:00:00'));

// Star them in the movie.
$movie->addActor($tomCruise);
$movie->addActor($mattDamon);

// Demo.
$actors = $movie->getActorsByAge();

// Output this out to the terminal.
echo json_encode($actors);