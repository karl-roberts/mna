<?php

namespace Entity;
use DateTimeInterface;
use Entity;
use Exception;
use JsonSerializable;
use DateTime;

class Movie implements EntityInterface, JsonSerializable
{
    /**
     * @var $id int The ID of the Movie.
     */
    private $id;


    /**
     * @var $title String The title of the Movie.
     */
    private $title;


    /**
     * @var $runTime int The runtime of the Movie in seconds
     */
    private $runTime;


    /**
     * @var $releaseDate DateTimeInterface The release date of the Movie.
     */
    private $releaseDate;


    /**
     * @var $actors Array A collections of actors that star in this movie.
     */
    private $actors;


    public function __construct()
    {
        $this->actors = [];
    }


    /**
     * @return int The ID of the Movie.
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * @return String The title of the Movie.
     */
    public function getTitle()
    {
        return $this->title;
    }


    /**
     * Sets the title of the Movie.
     * @param string $title The title of the Movie to set.
     * @throws Exception If the title supplied is not a string. If more than 40 characters in length
     */
    public function setTitle($title)
    {
        if (! is_string($title))
        {
            throw new Exception('Invalid title supplied');
        }
        else
        {
            if(strlen($title) > 40)
            {
                throw new Exception('Movie title cannot be more than 40 characters long');
            }

            $this->title = $title;
        }
    }


    /**
     * @return int The runtime of the Movie.
     */
    public function getRuntime()
    {
        return $this->runTime;
    }


    /**
     * Sets the runtime of the Movie.
     */
    public function setRuntime($runtime)
    {
        if (! is_int($runtime))
        {
            throw new Exception('Movie runtime must be specified in seconds');
        }
        else
        {
            $this->runTime = $runtime;
        }
    }


    /**
     * @return DateTimeInterface The release date of the Movie.
     */
    public function getReleaseDate()
    {
        return $this->releaseDate;
    }


    /**
     * Sets the release date for this Movie.
     * @param DateTimeInterface $releaseDate The release date of this Movie.
     * @throws Exception Thrown if invalid type is supplied.
     */
    public function setReleaseDate(DateTimeInterface $releaseDate)
    {
        if (! $releaseDate instanceof DateTimeInterface)
        {
            throw new Exception('Invalid type of date supplied');
        }
        else
        {
            $this->releaseDate = $releaseDate;
        }
    }


    /**
     * Add an Actor to this Movie.
     * @param Actor $actor An Actor to add to this movie.
     */
    public function addActor(Actor $actor)
    {
        $this->actors[] = $actor;
    }


    /**
     * Get the Actors that star in this Movie.
     * @return Array An array of Actor objects.
     */
    public function getActors()
    {
        return $this->actors;
    }


    /**
     * Get the Actors that star in this Movie by descending age.
     * @return Array an array of Actor objects.
     */
    public function getActorsByAge()
    {
        $actors = $this->getActors();

        usort($actors, function($a, $b) {
            $dateA = $a->getDateOfBirth();
            $dateB = $b->getDateOfBirth();

            if ($dateA == $dateB) {
                return 0;
            }

            return $dateA < $dateB ? 1 : -1;
        });

        return array_reverse($actors);
    }


    /**
     * (PHP 5 &gt;= 5.4.0)<br/>
     * Specify data which should be serialized to JSON
     * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     */
    function jsonSerialize()
    {
        $vars = get_object_vars($this);
        return $vars;
    }


    /**
     * @return string Returns the entity in JSON format.
     */
    function getJson()
    {
        return json_encode($this);
    }
}