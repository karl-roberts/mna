<?php

namespace Entity;

interface EntityInterface
{
    /**
     * @return int Returns the ID of this entity.
     */
    function getId();


    /**
     * @return string Returns the entity in JSON format.
     */
    function getJson();
}