<?php

namespace Entity;

use DateTimeInterface;
use Exception;
use JsonSerializable;

class Actor implements EntityInterface, JsonSerializable
{
    /**
     * @var $id int The ID of this Actor
     */
    private $id;


    /**
     * @var $name string The name of this Actor
     */
    private $name;


    /**
     * @var $dateOfBirth DateTimeInterface The date of birth of this actor
     */
    private $dateOfBirth;


    /**
     * @return int The ID of this Actor.
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * @return string The name of this Actor.
     */
    public function getName()
    {
        return $this->name;
    }


    /**
     * Sets the name for this Actor.
     * @param string $name The name to set.
     * @throws Exception Thrown if a string is not passed in or if it's longer than 40 characters.
     */
    public function setName($name)
    {
        if (! is_string($name))
        {
            throw new Exception('Invalid name supplied');
        }
        else
        {
            if(strlen($name) > 40)
            {
                throw new Exception('Actor name cannot be more than 40 characters in length');
            }

            $this->name = $name;
        }
    }


    /*
     * @return DateTimeInterface The date of birth of this Actor.
     */
    public function getDateOfBirth()
    {
        return $this->dateOfBirth;
    }


    /**
     * Sets the date of birth for this Actor.
     * @param DateTimeInterface $dateOfBirth The date of birth to set.
     * @throws Exception Thrown if invalid type is supplied.
     */
    public function setDateOfBirth(DateTimeInterface $dateOfBirth)
    {
        if (! $dateOfBirth instanceof DateTimeInterface)
        {
            throw new Exception('Invalid type of date supplied');
        }
        else
        {
            $this->dateOfBirth = $dateOfBirth;
        }
    }


    /**
     * (PHP 5 &gt;= 5.4.0)<br/>
     * Specify data which should be serialized to JSON
     * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     */
    function jsonSerialize()
    {
        $vars = get_object_vars($this);
        return $vars;
    }


    /**
     * @return string This Actor instance in JSON format.
     */
    public function getJson()
    {
        return json_encode($this);
    }
}