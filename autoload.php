<?php

define('BASE_PATH', realpath(dirname(__FILE__)));

function autoload($class)
{
    $filename = __DIR__ .'/'. str_replace('\\', '/', $class) . '.php';
    require_once($filename);
}

spl_autoload_register('autoload');